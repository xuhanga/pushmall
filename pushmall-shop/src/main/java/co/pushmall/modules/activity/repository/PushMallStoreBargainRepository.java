package co.pushmall.modules.activity.repository;

import co.pushmall.modules.activity.domain.PushMallStoreBargain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-12-22
 */
public interface PushMallStoreBargainRepository extends JpaRepository<PushMallStoreBargain, Integer>, JpaSpecificationExecutor {
}
