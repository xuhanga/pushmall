package co.pushmall.modules.activity.repository;

import co.pushmall.modules.activity.domain.PushMallUserExtract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-11-14
 */
public interface PushMallUserExtractRepository extends JpaRepository<PushMallUserExtract, Integer>, JpaSpecificationExecutor {
}
