package co.pushmall.modules.activity.service.impl;

import co.pushmall.modules.activity.domain.PushMallStoreSeckill;
import co.pushmall.modules.activity.repository.PushMallStoreSeckillRepository;
import co.pushmall.modules.activity.service.PushMallStoreSeckillService;
import co.pushmall.modules.activity.service.dto.PushMallStoreSeckillDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreSeckillQueryCriteria;
import co.pushmall.modules.activity.service.mapper.PushMallStoreSeckillMapper;
import co.pushmall.utils.OrderUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-12-14
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStoreSeckillServiceImpl implements PushMallStoreSeckillService {

    private final PushMallStoreSeckillRepository pushMallStoreSeckillRepository;

    private final PushMallStoreSeckillMapper pushMallStoreSeckillMapper;

    public PushMallStoreSeckillServiceImpl(PushMallStoreSeckillRepository pushMallStoreSeckillRepository, PushMallStoreSeckillMapper pushMallStoreSeckillMapper) {
        this.pushMallStoreSeckillRepository = pushMallStoreSeckillRepository;
        this.pushMallStoreSeckillMapper = pushMallStoreSeckillMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallStoreSeckillQueryCriteria criteria, Pageable pageable) {
        Page<PushMallStoreSeckill> page = pushMallStoreSeckillRepository.findAll((root, criteriaQuery, criteriaBuilder) ->
                QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        List<PushMallStoreSeckillDTO> storeSeckillDTOS = pushMallStoreSeckillMapper
                .toDto(page.getContent());
        int nowTime = OrderUtil.getSecondTimestampTwo();
        for (PushMallStoreSeckillDTO storeSeckillDTO : storeSeckillDTOS) {
            if (storeSeckillDTO.getStatus() > 0) {
                if (storeSeckillDTO.getStartTime() > nowTime) {
                    storeSeckillDTO.setStatusStr("活动未开始");
                } else if (storeSeckillDTO.getStopTime() < nowTime) {
                    storeSeckillDTO.setStatusStr("活动已结束");
                } else if (storeSeckillDTO.getStopTime() > nowTime && storeSeckillDTO.getStartTime() < nowTime) {
                    storeSeckillDTO.setStatusStr("正在进行中");
                }
            } else {
                storeSeckillDTO.setStatusStr("关闭");
            }

        }
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", storeSeckillDTOS);
        map.put("totalElements", page.getTotalElements());

        return map;
    }

    @Override
    public List<PushMallStoreSeckillDTO> queryAll(PushMallStoreSeckillQueryCriteria criteria) {
        return pushMallStoreSeckillMapper.toDto(pushMallStoreSeckillRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStoreSeckillDTO findById(Integer id) {
        Optional<PushMallStoreSeckill> yxStoreSeckill = pushMallStoreSeckillRepository.findById(id);
        ValidationUtil.isNull(yxStoreSeckill, "PushMallStoreSeckill", "id", id);
        return pushMallStoreSeckillMapper.toDto(yxStoreSeckill.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStoreSeckillDTO create(PushMallStoreSeckill resources) {
        return pushMallStoreSeckillMapper.toDto(pushMallStoreSeckillRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStoreSeckill resources) {
        Optional<PushMallStoreSeckill> optionalPushMallStoreSeckill = pushMallStoreSeckillRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStoreSeckill, "PushMallStoreSeckill", "id", resources.getId());
        PushMallStoreSeckill pushMallStoreSeckill = optionalPushMallStoreSeckill.get();
        pushMallStoreSeckill.copy(resources);
        pushMallStoreSeckillRepository.save(pushMallStoreSeckill);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallStoreSeckillRepository.deleteById(id);
    }
}
