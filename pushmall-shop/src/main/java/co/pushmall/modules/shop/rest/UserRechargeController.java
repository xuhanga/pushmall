package co.pushmall.modules.shop.rest;

import co.pushmall.aop.log.Log;
import co.pushmall.modules.shop.domain.PushMallUserRecharge;
import co.pushmall.modules.shop.service.PushMallUserRechargeService;
import co.pushmall.modules.shop.service.dto.PushMallUserRechargeQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author pushmall
 * @date 2020-03-02
 */
@Api(tags = "充值管理管理")
@RestController
@RequestMapping("/api/PmUserRecharge")
public class UserRechargeController {

    private final PushMallUserRechargeService pushMallUserRechargeService;

    public UserRechargeController(PushMallUserRechargeService pushMallUserRechargeService) {
        this.pushMallUserRechargeService = pushMallUserRechargeService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('yxUserRecharge:list')")
    public void download(HttpServletResponse response, PushMallUserRechargeQueryCriteria criteria) throws IOException {
        pushMallUserRechargeService.download(pushMallUserRechargeService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询充值管理")
    @ApiOperation("查询充值管理")
    @PreAuthorize("@el.check('yxUserRecharge:list')")
    public ResponseEntity<Object> getPushMallUserRecharges(PushMallUserRechargeQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity<>(pushMallUserRechargeService.queryAll(criteria, pageable), HttpStatus.OK);
    }

    @PostMapping
    @Log("新增充值管理")
    @ApiOperation("新增充值管理")
    @PreAuthorize("@el.check('yxUserRecharge:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody PushMallUserRecharge resources) {
        return new ResponseEntity<>(pushMallUserRechargeService.create(resources), HttpStatus.CREATED);
    }


    @Log("删除充值管理")
    @ApiOperation("删除充值管理")
    @PreAuthorize("@el.check('yxUserRecharge:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody Integer[] ids) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallUserRechargeService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
