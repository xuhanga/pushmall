package co.pushmall.modules.shop.rest;

import co.pushmall.aop.log.Log;
import co.pushmall.modules.shop.domain.PushMallSystemUserTask;
import co.pushmall.modules.shop.service.PushMallSystemUserTaskService;
import co.pushmall.modules.shop.service.dto.PushMallSystemUserTaskQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-12-04
 */
@Api(tags = "商城:用户任务管理")
@RestController
@RequestMapping("api")
public class SystemUserTaskController {

    private final PushMallSystemUserTaskService pushMallSystemUserTaskService;

    public SystemUserTaskController(PushMallSystemUserTaskService pushMallSystemUserTaskService) {
        this.pushMallSystemUserTaskService = pushMallSystemUserTaskService;
    }

    @Log("查询")
    @ApiOperation(value = "查询")
    @GetMapping(value = "/PmSystemUserTask")
    @PreAuthorize("@el.check('admin','YXSYSTEMUSERTASK_ALL','YXSYSTEMUSERTASK_SELECT')")
    public ResponseEntity getPushMallSystemUserTasks(PushMallSystemUserTaskQueryCriteria criteria,
                                                     Pageable pageable) {
        Sort sort = new Sort(Sort.Direction.ASC, "levelId");
        Pageable pageableT = PageRequest.of(pageable.getPageNumber(),
                pageable.getPageSize(),
                sort);
        return new ResponseEntity(pushMallSystemUserTaskService.queryAll(criteria, pageableT),
                HttpStatus.OK);
    }

    @Log("新增")
    @ApiOperation(value = "新增")
    @PostMapping(value = "/PmSystemUserTask")
    @PreAuthorize("@el.check('admin','YXSYSTEMUSERTASK_ALL','YXSYSTEMUSERTASK_CREATE')")
    public ResponseEntity create(@Validated @RequestBody PushMallSystemUserTask resources) {
        return new ResponseEntity(pushMallSystemUserTaskService.create(resources), HttpStatus.CREATED);
    }

    @Log("修改")
    @ApiOperation(value = "修改")
    @PutMapping(value = "/PmSystemUserTask")
    @PreAuthorize("@el.check('admin','YXSYSTEMUSERTASK_ALL','YXSYSTEMUSERTASK_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallSystemUserTask resources) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallSystemUserTaskService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除")
    @ApiOperation(value = "删除")
    @DeleteMapping(value = "/PmSystemUserTask/{id}")
    @PreAuthorize("@el.check('admin','YXSYSTEMUSERTASK_ALL','YXSYSTEMUSERTASK_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallSystemUserTaskService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
