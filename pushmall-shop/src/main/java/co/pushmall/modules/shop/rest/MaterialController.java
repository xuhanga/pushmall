package co.pushmall.modules.shop.rest;


import co.pushmall.aop.log.Log;
import co.pushmall.modules.shop.domain.PushMallMaterial;
import co.pushmall.modules.shop.service.PushMallMaterialService;
import co.pushmall.modules.shop.service.dto.PushMallMaterialQueryCriteria;
import co.pushmall.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2020-01-09
 */
@Api(tags = "商城:素材管理管理")
@RestController
@RequestMapping("/api/material")
public class MaterialController {

    private final PushMallMaterialService pushMallMaterialService;

    public MaterialController(PushMallMaterialService pushMallMaterialService) {
        this.pushMallMaterialService = pushMallMaterialService;
    }


    @GetMapping(value = "/page")
    @Log("查询素材管理")
    @ApiOperation("查询素材管理")
    public ResponseEntity<Object> getPushMallMaterials(PushMallMaterialQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity<>(pushMallMaterialService.queryAll(criteria, pageable), HttpStatus.OK);
    }

    @PostMapping
    @Log("新增素材管理")
    @ApiOperation("新增素材管理")
    public ResponseEntity<Object> create(@Validated @RequestBody PushMallMaterial resources) {
        resources.setCreateId(SecurityUtils.getUsername());
        return new ResponseEntity<>(pushMallMaterialService.create(resources), HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改素材管理")
    @ApiOperation("修改素材管理")
    public ResponseEntity<Object> update(@Validated @RequestBody PushMallMaterial resources) {
        pushMallMaterialService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除素材管理")
    @ApiOperation("删除素材管理")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> deleteAll(@PathVariable String id) {
        pushMallMaterialService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
