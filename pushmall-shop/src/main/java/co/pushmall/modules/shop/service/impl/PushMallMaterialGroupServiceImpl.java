package co.pushmall.modules.shop.service.impl;

import cn.hutool.core.util.IdUtil;
import co.pushmall.modules.shop.domain.PushMallMaterialGroup;
import co.pushmall.modules.shop.repository.PushMallMaterialGroupRepository;
import co.pushmall.modules.shop.service.PushMallMaterialGroupService;
import co.pushmall.modules.shop.service.dto.PushMallMaterialGroupDto;
import co.pushmall.modules.shop.service.dto.PushMallMaterialGroupQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallMaterialGroupMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * @author pushmall
 * @date 2020-01-09
 */
@Service
//@CacheConfig(cacheNames = "yxMaterialGroup")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallMaterialGroupServiceImpl implements PushMallMaterialGroupService {

    private final PushMallMaterialGroupRepository pushMallMaterialGroupRepository;

    private final PushMallMaterialGroupMapper pushMallMaterialGroupMapper;

    public PushMallMaterialGroupServiceImpl(PushMallMaterialGroupRepository pushMallMaterialGroupRepository, PushMallMaterialGroupMapper pushMallMaterialGroupMapper) {
        this.pushMallMaterialGroupRepository = pushMallMaterialGroupRepository;
        this.pushMallMaterialGroupMapper = pushMallMaterialGroupMapper;
    }

    @Override
    //@Cacheable
    public Map<String, Object> queryAll(PushMallMaterialGroupQueryCriteria criteria, Pageable pageable) {
        Page<PushMallMaterialGroup> page = pushMallMaterialGroupRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallMaterialGroupMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<PushMallMaterialGroupDto> queryAll(PushMallMaterialGroupQueryCriteria criteria) {
        return pushMallMaterialGroupMapper.toDto(pushMallMaterialGroupRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public PushMallMaterialGroupDto findById(String id) {
        PushMallMaterialGroup pushMallMaterialGroup = pushMallMaterialGroupRepository.findById(id).orElseGet(PushMallMaterialGroup::new);
        ValidationUtil.isNull(pushMallMaterialGroup.getId(), "PushMallMaterialGroup", "id", id);
        return pushMallMaterialGroupMapper.toDto(pushMallMaterialGroup);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public PushMallMaterialGroupDto create(PushMallMaterialGroup resources) {
        resources.setId(IdUtil.simpleUUID());
        return pushMallMaterialGroupMapper.toDto(pushMallMaterialGroupRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallMaterialGroup resources) {
        PushMallMaterialGroup pushMallMaterialGroup = pushMallMaterialGroupRepository.findById(resources.getId()).orElseGet(PushMallMaterialGroup::new);
        ValidationUtil.isNull(pushMallMaterialGroup.getId(), "PushMallMaterialGroup", "id", resources.getId());
        pushMallMaterialGroup.copy(resources);
        pushMallMaterialGroupRepository.save(pushMallMaterialGroup);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        for (String id : ids) {
            pushMallMaterialGroupRepository.deleteById(id);
        }
    }

    @Override
    public void deleteById(String id) {
        pushMallMaterialGroupRepository.deleteById(id);
    }
}
