package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallMaterialGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2020-01-09
 */
public interface PushMallMaterialGroupRepository extends JpaRepository<PushMallMaterialGroup, String>, JpaSpecificationExecutor<PushMallMaterialGroup> {
}
