package co.pushmall.modules.shop.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.shop.domain.PushMallSystemGroupData;
import co.pushmall.modules.shop.service.dto.PushMallSystemGroupDataDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-10-18
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallSystemGroupDataMapper extends EntityMapper<PushMallSystemGroupDataDTO, PushMallSystemGroupData> {

}
