package co.pushmall.modules.shop.service.impl;

import cn.hutool.core.util.IdUtil;
import co.pushmall.modules.shop.domain.PushMallMaterial;
import co.pushmall.modules.shop.repository.PushMallMaterialRepository;
import co.pushmall.modules.shop.service.PushMallMaterialService;
import co.pushmall.modules.shop.service.dto.PushMallMaterialDto;
import co.pushmall.modules.shop.service.dto.PushMallMaterialQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallMaterialMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2020-01-09
 */
@Service
//@CacheConfig(cacheNames = "yxMaterial")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallMaterialServiceImpl implements PushMallMaterialService {

    private final PushMallMaterialRepository pushMallMaterialRepository;

    private final PushMallMaterialMapper pushMallMaterialMapper;

    public PushMallMaterialServiceImpl(PushMallMaterialRepository pushMallMaterialRepository, PushMallMaterialMapper pushMallMaterialMapper) {
        this.pushMallMaterialRepository = pushMallMaterialRepository;
        this.pushMallMaterialMapper = pushMallMaterialMapper;
    }

    @Override
    //@Cacheable
    public Map<String, Object> queryAll(PushMallMaterialQueryCriteria criteria, Pageable pageable) {
        Page<PushMallMaterial> page = pushMallMaterialRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallMaterialMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<PushMallMaterialDto> queryAll(PushMallMaterialQueryCriteria criteria) {
        return pushMallMaterialMapper.toDto(pushMallMaterialRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public PushMallMaterialDto findById(String id) {
        PushMallMaterial pushMallMaterial = pushMallMaterialRepository.findById(id).orElseGet(PushMallMaterial::new);
        ValidationUtil.isNull(pushMallMaterial.getId(), "PushMallMaterial", "id", id);
        return pushMallMaterialMapper.toDto(pushMallMaterial);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public PushMallMaterialDto create(PushMallMaterial resources) {
        resources.setId(IdUtil.simpleUUID());
        return pushMallMaterialMapper.toDto(pushMallMaterialRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallMaterial resources) {
        PushMallMaterial pushMallMaterial = pushMallMaterialRepository.findById(resources.getId()).orElseGet(PushMallMaterial::new);
        ValidationUtil.isNull(pushMallMaterial.getId(), "PushMallMaterial", "id", resources.getId());
        pushMallMaterial.copy(resources);
        pushMallMaterialRepository.save(pushMallMaterial);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        for (String id : ids) {
            pushMallMaterialRepository.deleteById(id);
        }
    }

    @Override
    public void deleteById(String id) {
        pushMallMaterialRepository.deleteById(id);
    }
}
