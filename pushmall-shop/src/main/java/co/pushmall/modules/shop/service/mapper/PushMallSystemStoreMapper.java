package co.pushmall.modules.shop.service.mapper;

import co.pushmall.base.BaseMapper;
import co.pushmall.modules.shop.domain.PushMallSystemStore;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2020-03-03
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallSystemStoreMapper extends BaseMapper<PushMallSystemStoreDto, PushMallSystemStore> {

}
