package co.pushmall.mp.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.mp.domain.PushMallWechatMenu;
import co.pushmall.mp.service.dto.PushMallWechatMenuDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-10-06
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallWechatMenuMapper extends EntityMapper<PushMallWechatMenuDTO, PushMallWechatMenu> {

}
