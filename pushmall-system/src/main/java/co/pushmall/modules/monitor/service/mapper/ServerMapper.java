package co.pushmall.modules.monitor.service.mapper;

import co.pushmall.base.BaseMapper;
import co.pushmall.modules.monitor.domain.Server;
import co.pushmall.modules.monitor.service.dto.ServerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author Zhang houying
 * @date 2019-11-03
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ServerMapper extends BaseMapper<ServerDTO, Server> {

}
